<!-- markdownlint-disable MD033-->

<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

姓名：冷俊雪   班级：软件1班   学号：202010112313

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

这里是一个基于Oracle数据库的商品销售系统的数据库设计方案:

## 表空间和表设计:

- 创建两个表空间:用户表空间用户数据,索引表空间用户索引  
  
  ```sql
  -- 创建用户数据表空间 
  CREATE TABLESPACE userdata 
  DATAFILE 'userdata.dbf' 
  SIZE 100M AUTOEXTEND ON;
  
  -- 创建用户索引表空间
  CREATE TABLESPACE userindex 
  DATAFILE 'userindex.dbf' 
  SIZE 50M AUTOEXTEND ON;
  ```
  
  这两个表空间分别用于存放用户表的数据和索引。数据表空间设置初始大小100M,索引表空间设置初始大小50M,并都设置自动扩展,以容纳不断增长的数据。
  
  

- 创建4张表:  
  -- 商品表products: 包含商品编号、名称、价格、库存等
  -- 客户表customers: 包含客户编号、姓名、地址等
  -- 订单表orders: 包含订单编号、客户编号、下单日期等
  -- 订单详情表order_items: 包含订单编号、商品编号、数量、单价等
  
  ```sql
  -- 商品表
  CREATE TABLE products (
    pid NUMBER(6) NOT NULL ,  
    pname VARCHAR2(40),
    price NUMBER(8,2),  
    stock NUMBER(4),
    CONSTRAINT products_pk PRIMARY KEY (pid)
      USING INDEX TABLESPACE userindex  
  ) 
  TABLESPACE userdata;
  
  -- 客户表 
  CREATE TABLE customers (
    cid NUMBER(6) NOT NULL ,  
    cname VARCHAR2(40),
    address VARCHAR2(200),
    CONSTRAINT customers_pk PRIMARY KEY (cid)  
  )
  TABLESPACE userdata;
  
  -- 订单表
  CREATE TABLE orders (
    oid NUMBER(6) NOT NULL ,      
    cid NUMBER(6) NOT NULL , 
    odate DATE,
    CONSTRAINT orders_pk PRIMARY KEY (oid)  ,
    CONSTRAINT fk_customers 
      FOREIGN KEY (cid) REFERENCES customers(cid)
  )
  TABLESPACE userdata;
  
  -- 订单详情表
  CREATE TABLE order_items (
    oid NUMBER(6) NOT NULL ,  
    pid NUMBER(6) NOT NULL , 
    num NUMBER(4) ,
    price NUMBER(8,2), 
    CONSTRAINT fk_orders 
      FOREIGN KEY (oid) REFERENCES orders(oid),
    CONSTRAINT fk_products
      FOREIGN KEY (pid) REFERENCES products(pid)
  )  
  TABLESPACE userdata; 
  ```
  
  

- 插入至少10万条模拟数据
  
  ```sql
  -- 批量插入商品数据
  INSERT INTO products(pid, pname, price, stock)  
  SELECT rownum, '产品'||TO_CHAR(rownum), 
         dbms_random.value(100,200), 
         dbms_random.value(50,200)
  FROM dual 
  CONNECT BY level <= 10000; 
  
  -- 批量插入客户数据 
  INSERT INTO customers(cid, cname, address)
  SELECT rownum, '客户'||TO_CHAR(rownum), 
         '地址'||TO_CHAR(rownum)  
  FROM dual
  CONNECT BY level <= 10000;
  
  -- 批量插入订单数据
  INSERT INTO orders(oid, cid, odate)
  SELECT rownum, dbms_random.value(1,10000),
         TRUNC(SYSDATE - dbms_random.value(1,365))  odate  
  FROM dual  
  CONNECT BY level <= 10000;
  
  -- 批量插入订单详情数据
  INSERT INTO order_items(oid, pid, num, price)  
  SELECT o.oid, 
         dbms_random.value(1,10000),  
         dbms_random.value(1,10),  
         p.price  
  FROM orders o, products p  
  WHERE o.cid = p.pid  
  CONNECT BY level <= 100000; 
  ```

## 权限和用户设计:

- 创建两个用户:UMANAGER管理员,USALES销售人员  
- UMANAGER赋予DBA权限,USALES赋予插入、删除、修改商品、客户、订单等表的权限

```sql
-- 创建UMANAGER用户
CREATE USER UMANAGER IDENTIFIED BY password DEFAULT TABLESPACE userdata TEMPORARY TABLESPACE temp;
GRANT DBA TO UMANAGER;

-- 创建USALES用户
CREATE USER USALES IDENTIFIED BY password DEFAULT TABLESPACE userdata TEMPORARY TABLESPACE temp;
GRANT INSERT, DELETE, UPDATE ON products TO USALES;
GRANT INSERT, DELETE, UPDATE ON customers TO USALES;
GRANT INSERT, DELETE, UPDATE ON orders TO USALES;
GRANT INSERT, DELETE, UPDATE ON order_items TO USALES;
```

- 创建一个程序包PACK_SALE来存放存储过程和函数  

- 设计存储过程计算订单金额、修改商品价格、生成订单编号  

- 设计函数计算每个客户的订单总金额、每个商品的销售总额
  
  ```sql
  CREATE OR REPLACE PACKAGE PACK_SALE AS
    -- 存储过程：计算订单金额
    PROCEDURE calculate_order_amount(p_order_id IN NUMBER);
  
    -- 存储过程：修改商品价格
    PROCEDURE update_product_price(p_product_id IN NUMBER, p_new_price IN NUMBER);
  
    -- 存储过程：生成订单编号
    PROCEDURE generate_order_id(p_customer_id IN NUMBER);
  
    -- 函数：计算每个客户的订单总金额
    FUNCTION calculate_total_order_amount(p_customer_id IN NUMBER) RETURN NUMBER;
  
    -- 函数：计算每个商品的销售总额
    FUNCTION calculate_total_sales_amount(p_product_id IN NUMBER) RETURN NUMBER;
  END PACK_SALE;
  /
  
  CREATE OR REPLACE PACKAGE BODY PACK_SALE AS
    -- 存储过程：计算订单金额
    PROCEDURE calculate_order_amount(p_order_id IN NUMBER) AS
      v_order_amount NUMBER;
    BEGIN
      -- 在这里编写计算订单金额的逻辑，将结果存储到 v_order_amount 变量中
  
      -- 示例逻辑：假设订单金额为订单详情表中数量（quantity）乘以单价（price）的总和
      SELECT SUM(quantity * price) INTO v_order_amount
      FROM order_items
      WHERE order_id = p_order_id;
  
      -- 输出订单金额
      DBMS_OUTPUT.PUT_LINE('Order Amount: ' || v_order_amount);
    END calculate_order_amount;
  
    -- 存储过程：修改商品价格
    PROCEDURE update_product_price(p_product_id IN NUMBER, p_new_price IN NUMBER) AS
    BEGIN
      -- 在这里编写修改商品价格的逻辑
  
      -- 示例逻辑：更新商品表中的价格字段
      UPDATE products
      SET price = p_new_price
      WHERE product_id = p_product_id;
    END update_product_price;
  
    -- 存储过程：生成订单编号
    PROCEDURE generate_order_id(p_customer_id IN NUMBER) AS
      v_order_id NUMBER;
    BEGIN
      -- 在这里编写生成订单编号的逻辑，将结果存储到 v_order_id 变量中
  
      -- 示例逻辑：假设订单编号为自增长序列的下一个值
      SELECT order_id_seq.NEXTVAL INTO v_order_id
      FROM dual;
  
      -- 插入新的订单记录
      INSERT INTO orders(order_id, customer_id, order_date)
      VALUES (v_order_id, p_customer_id, SYSDATE);
  
      -- 输出生成的订单编号
      DBMS_OUTPUT.PUT_LINE('Generated Order ID: ' || v_order_id);
    END generate_order_id;
  
    -- 函数：计算每个客户的订单总金额
    FUNCTION calculate_total_order_amount(p_customer_id IN NUMBER) RETURN NUMBER AS
      v_total_amount NUMBER;
    BEGIN
      -- 在这里编写计算每个客户的订单总金额的逻辑，将结果存储到 v_total_amount 变量中
  
      -- 示例逻辑：计算订单详情表中某客户的订单金额总和
      SELECT SUM(quantity * price) INTO v_total_amount
      FROM order_items oi
      INNER JOIN orders o ON oi.order_id = o.order_id
      WHERE o.customer_id = p_customer_id;
  
      -- 返回订单总金额
      RETURN v_total_amount;
    END calculate_total_order_amount;
  
    -- 函数：计算每个商品的销售总额
    FUNCTION calculate_total_sales_amount(p_product_id IN NUMBER) RETURN NUMBER AS
      v_total_amount NUMBER;
    BEGIN
      -- 在这里编写计算每个商品的销售总额的逻辑，将结果存储到 v_total_amount 变量中
  
      -- 示例逻辑：计算订单详情表中某商品的销售总额
      SELECT SUM(quantity * price) INTO v_total_amount
      FROM order_items
      WHERE product_id = p_product_id;
  
      -- 返回销售总额
      RETURN v_total_amount;
    END calculate_total_sales_amount;
  END PACK_SALE;
  /
  
  ```

- 每周备份一次数据库,将备份文件转存至异地DR环境  
  
  ```bash
  # backup_script.sh (Linux/Unix)
  
  # 导出数据库
  expdp username/password DIRECTORY=data_pump_dir DUMPFILE=database_backup.dmp FULL=YES
  
  # 将备份文件拷贝到异地DR环境（假设DR环境挂载在 /mnt/dr 目录下）
  cp database_backup.dmp /mnt/dr/
  
  ```

- 备份文件使用时间戳并压缩  
  
  ```bash
  # backup_script.sh (Linux/Unix)
  
  # 生成时间戳
  timestamp=$(date +%Y%m%d%H%M%S)
  
  # 导出数据库
  expdp username/password DIRECTORY=data_pump_dir DUMPFILE=database_backup_${timestamp}.dmp FULL=YES
  
  # 压缩备份文件
  gzip database_backup_${timestamp}.dmp
  
  # 将压缩后的备份文件拷贝到异地DR环境（假设DR环境挂载在 /mnt/dr 目录下）
  cp database_backup_${timestamp}.dmp.gz /mnt/dr/
  
  ```
  
  实验总结与体会：
  在设计基于Oracle数据库的商品销售系统的数据库方案时，我遵循了以下步骤：

表空间和表设计：创建了两个表空间，一个用于存储用户数据，另一个用于存储索引。创建了四张表：商品表（products）、客户表（customers）、
订单表（orders）和订单详情表（order_items）。这些表包含了与商品销售系统相关的必要字段，如商品编号、名称、价格、库存，客户编号、姓名、
地址，订单编号、客户编号、下单日期等。

模拟数据插入：通过批量插入语句向表中插入了至少10万条模拟数据，确保数据库具有足够的数据量进行实际操作和测试。

权限和用户设计：创建了两个用户，UMANAGER和USALES。UMANAGER用户被授予DBA权限，具有对数据库的完全控制权。USALES用户被授予对商品、客户、订单和订单详情等表的插入、删除和修改权限，
以便销售人员能够进行相应的操作。

程序包和存储过程设计：创建了一个名为PACK_SALE的程序包，用于存放存储过程和函数。在程序包中，设计了一些存储过程和函数来实现复杂的业务逻辑，
如计算订单金额、修改商品价格、生成订单编号等。这些存储过程和函数通过PL/SQL语言编写，并通过调用它们来实现所需的功能。

备份方案设计：设计了一个简单的备份方案。每周执行一次数据库备份，使用Oracle的数据泵工具（expdp）将数据库导出为备份文件。
备份文件使用时间戳命名并进行压缩，以便更好地管理和存储。最后，将压缩后的备份文件复制到异地DR（灾难恢复）环境，以提供额外的数据安全性和冗余。

通过以上设计方案，我们建立了一个基于Oracle数据库的商品销售系统，包括必要的表结构、模拟数据、用户权限、复杂业务逻辑的存储过程和函数，
以及简单但可靠的数据库备份方案。这样的设计使系统能够支持商品的管理、客户的管理、订单的管理以及相关的业务逻辑处理，并提供数据的安全性和可靠性保障。
  
  
  
  

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - test6_design.docx学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项      | 评分标准               | 满分  |
|:-------- |:------------------ |:--- |
| 文档整体     | 文档内容详实、规范，美观大方     | 10  |
| 表设计      | 表设计及表空间设计合理，样例数据合理 | 20  |
| 用户管理     | 权限及用户分配方案设计正确      | 20  |
| PL/SQL设计 | 存储过程和函数设计正确        | 30  |
| 备份方案     | 备份方案设计正确           | 20  |
