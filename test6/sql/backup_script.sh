# backup_script.sh (Linux/Unix)

# 导出数据库
expdp username/password DIRECTORY=data_pump_dir DUMPFILE=database_backup.dmp FULL=YES

# 将备份文件拷贝到磁带（假设磁带挂载在 /mnt/tape 目录下）
cp database_backup.dmp /mnt/tape/

# 生成时间戳
timestamp=$(date +%Y%m%d%H%M%S)

# 导出数据库
expdp username/password DIRECTORY=data_pump_dir DUMPFILE=database_backup_${timestamp}.dmp FULL=YES

# 压缩备份文件
gzip database_backup_${timestamp}.dmp

# 将压缩后的备份文件拷贝到异地DR环境（假设DR环境挂载在 /mnt/dr 目录下）
cp database_backup_${timestamp}.dmp.gz /mnt/dr/
